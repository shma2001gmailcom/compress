#!/bin/sh
JAVA_HOME=/home/misha/.jdks/jbrsdk_jcef-17.0.9
export JAVA_HOME
java=${JAVA_HOME}/bin/java
M2_HOME='/snap/intellij-idea-community/502/plugins/maven/lib/maven3'
export M2_HOME
mvn=${M2_HOME}/bin/mvn
cd ../
rm -r target
${mvn} clean install -X
cd target || exit 1
${java} -jar compress.jar
