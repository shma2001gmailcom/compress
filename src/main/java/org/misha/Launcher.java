package org.misha;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import static java.lang.Thread.currentThread;

/**
 * author: misha
 * date: 4/19/18
 */
public class Launcher {
    private static final Logger log = LoggerFactory.getLogger(Launcher.class);
    private static final Properties properties = makeProperties();
    public static final String SEPARATOR = properties.getProperty("hash.encoded.text.delimiter");

    public static void main(String... args) {
        final Compressor compressor = new Compressor(readLiBo());
        compressor.compress();
        log.debug(compressor.compressed());
    }

    private static String readLiBo() {
        StringBuilder sb = new StringBuilder();
        try (InputStream is = readResource("Li-Bo.txt");
             InputStreamReader fr = new InputStreamReader(is);
             BufferedReader br = new BufferedReader(fr)
        ) {
            String line;
            while ((line = br.readLine()) != null) sb.append(line);
        } catch (IOException e) {
            //
        }
        return sb.toString();
    }

    private static InputStream readResource(String name) {
        return currentThread().getContextClassLoader().getResourceAsStream(name);
    }

    private static Properties makeProperties() {
        final Properties properties = new Properties();
        try (InputStream is = readResource("application.properties")) {
            properties.load(is);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return properties;
    }
}
