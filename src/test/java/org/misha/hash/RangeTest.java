package org.misha.hash;

import org.junit.jupiter.api.Test;
import org.misha.TestLiBo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author: misha
 * date: 2/23/18
 * time: 6:45 PM
 */
public class RangeTest {
    private static final Logger log = LoggerFactory.getLogger(TestLiBo.class);

    @Test
    public void getRandom() {
        Range range = new Range(1111111L, 77777777L);
        for(int i = 0; i < 1000; ++i, log.debug("next random: {}", range.getRandom()));
    }
}